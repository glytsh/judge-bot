# Judge bot


#### --- EN

Judge_Bot is a bot created specifically for the Force&Houblons guild, which is a guild in the game Albion Online. This bot (somewhere between a bot and a script) aims to launch votes automatically on Discord based on players' seniority by writing text, reactions, and a public thread.

Currently, the bot is launched every week via a deployment pipeline.

The information to be modified for your server is as follows:
ROLE_ID_TO_VERIFY = 1095052297723195422
fonda_id = 1089235073867448501
bras_droit_id = 1089236449196511383
membre_id = 1089235862342095069

Replace the sequences of numbers with the IDs of your roles.

TEXT_CHANNEL_ID = 1114629649784393818

Replace the sequence of numbers with the ID of the chosen channel

guild = bot.get_guild(1089220929755824230)

Replace the sequence of number with the ID of your guild

#### --- FR 

Le Judge_Bot est un bot réalisé spécifiquement pour la guilde Force&Houblons, qui est une guilde du jeu Albion Online. 
Ce bot(à mis chemin entre un bot et un script) à pour but de lancer des votes automatiquement sur discord en fonction de l'ancienneté des joueurs, en écrivant du texte, des réactions  et un fil publique. 

Actuellement le bot est lancé toutes les semaines via une pipeline de déploiement. 

Les informations à modifier pour votre serveur sont celles-ci : 
ROLE_ID_A_VERIFIER = 1095052297723195422 
fonda_id = 1089235073867448501
bras_droit_id = 1089236449196511383
membre_id = 1089235862342095069

Remplacer les suites de chiffres par l'id de vos rôles 

SALON_TEXTUEL_ID = 1114629649784393818 

Remplacer les suites de chiffres par l'id du salon choisi 

guild = bot.get_guild(1089220929755824230)

Remplacer les suites de chiffres par l'id de la guilde

## How to start it / Comment le lancer 

#### --- EN

To launch it, simply run the deployment pipeline provided in the source code.

#### --- FR

Pour le lancer il faut simplement lancer la pipeline de déploiement mis à disposition dans le code source.
