import discord
from discord.ext import commands, tasks
from datetime import datetime, timedelta, timezone
import sys
import asyncio


app_discord_token = sys.argv[1]

intents = discord.Intents.all()
# intents.typing = True
# intents.presences = True

# Remplacez "TOKEN_DU_BOT" par le token de votre bot Discord
bot = commands.Bot(command_prefix="!",
                   description="Bot d'ancienneté des joueurs",
                   intents=intents)

# ID des rôles controlés 
ROLE_ID_A_VERIFIER = 1095052297723195422
fonda_id = 1089235073867448501
bras_droit_id = 1089236449196511383
membre_id = 1089235862342095069

# ID du salon textuel où le bot enverra le message
SALON_TEXTUEL_ID = 1114629649784393818


@bot.event
async def on_ready():
  print(f'Connecté en tant que {bot.user.name}')
  print('------')
  verifier_anciennete.start()

@tasks.loop(count=1)  # Exécute la tâche toutes les semaines
async def verifier_anciennete():
  guild = bot.get_guild(1089220929755824230)
  if not guild:
    return
    bot.close()  # Ferme le bot si le serveur n'est pas trouvé

  role = discord.utils.get(guild.roles, id=ROLE_ID_A_VERIFIER)
  fonda = discord.utils.get(guild.roles, id=fonda_id)
  bras_droit = discord.utils.get(guild.roles, id=bras_droit_id)
  membre = discord.utils.get(guild.roles, id=membre_id)

  if not role:
    print("Le rôle spécifié n'existe pas.")
    return
    bot.close()  # Ferme le bot si le role n'est pas trouvé

  ancien_role = discord.utils.get(guild.roles, name="Membre")
  if not ancien_role:
    ancien_role = await guild.create_role(name="Membre")

  probation = discord.utils.get(guild.roles, name="Probation")
  if not probation:
    probation = await guild.create_role(name="Probation")

  for member in guild.members:
    thread_name = member.name
    if role in member.roles:
      difference = datetime.now(timezone.utc) - member.joined_at
      if difference >= timedelta(days=14) and ancien_role not in member.roles and probation in member.roles:
        salon_textuel = bot.get_channel(SALON_TEXTUEL_ID)
        message = f"{membre.mention} {bras_droit.mention} {fonda.mention} \nSuite à la prolongation décidée en majorité les votes pour {member.mention} sont lancés ! \n\n❌  = Refus de           rejoindre la guilde\n✅ = Validation de l'adhésion \n\nMerci d'argumenter un minimum votre vote dans le fil de discussion.\nSi le refus à été voté, ce membre sera kick de la guilde."
        sended_message = await salon_textuel.send(message)
        await sended_message.create_thread(name=thread_name,
                                           auto_archive_duration=0)
        await sended_message.add_reaction('❌')
        await sended_message.add_reaction('✅')

  for member in guild.members:
    thread_name = member.name
    if role in member.roles:
      difference = datetime.now(timezone.utc) - member.joined_at
      if difference >= timedelta(days=7) and ancien_role not in member.roles and probation not in member.roles:
        salon_textuel = bot.get_channel(SALON_TEXTUEL_ID)
        message = f"{membre.mention} {bras_droit.mention} {fonda.mention} \nLes votes pour {member.mention} sont lancés ! \n\n❌  = Refus de           rejoindre la guilde\n❓ = Prolongation de la période d'essai\n✅ = Validation de l'adhésion \n\nMerci d'argumenter un minimum votre vote dans le fil de discussion"
        sended_message = await salon_textuel.send(message)
        await sended_message.create_thread(name=thread_name,
                                           auto_archive_duration=0)
        await sended_message.add_reaction('❌')
        await sended_message.add_reaction('❓')
        await sended_message.add_reaction('✅')
  bot.close()  # Ferme le bot après avoir terminé la tâche
  exit() # Arrête de script
bot.run(app_discord_token)